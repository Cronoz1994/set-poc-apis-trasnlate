import {Component, OnInit} from '@angular/core';

import {AmazonTranslateService} from './services/amazon-translate.service';
import {GoogleCloudTranslateService} from './services/google-cloud-translate.service';
import {GoogleCloudGetLanguagesService} from './services/google-cloud-get-languages.service';
import {API_SERVICES, ApiService, SERVICE_CODE} from './constants/api-services.constant';
import {GoogleCloudLanguageResponse, GoogleCloudLanguagesResponse} from './models/google-cloud-language.response';
import {AMAZON_TRANSLATE_LANGUAGES, AmazonLanguage} from './constants/amazon-translate-languages.constant';
import {AmazonTranslationRequestModel} from './models/amazon-translation-request.model';
import {AmazonTranslationResponseModel} from './models/amazon-translation-response.model';
import {GoogleCloudTranslateRequestModel} from './models/google-cloud-translate-request.model';
import {GoogleCloudTranslateResponseModel, GoogleCloudTranslationsResponseModel} from './models/google-cloud-translate-response.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public title = 'Translate example';

  public inputText: string;
  public translatedText: string;
  public previewLanguageSelected: string;

  public serviceSelected: string;
  public languagesSupported: { name?: string, value: string } [];

  public readonly MAX_CHAR_LENGTH = 5000;
  public readonly API_SERVICES: ApiService [] = API_SERVICES;

  constructor(private _googleCloudGetLanguagesService: GoogleCloudGetLanguagesService,
              private _googleCloudTranslateService: GoogleCloudTranslateService,
              private _amazonTranslateService: AmazonTranslateService) {
    this.serviceSelected = SERVICE_CODE.AMAZON;
    this.previewLanguageSelected = 'es';
    this.inputText = 'Hello, world!';
  }

  ngOnInit(): void {
    this._amazonServiceConfig();
  }

  public translateText(): void {
    switch (this.serviceSelected) {
      case SERVICE_CODE.GOOGLE_CLOUD: {
        this._googleCloudServiceTranslate();
        break;
      }

      case SERVICE_CODE.AMAZON: {
        this._amazonServiceTranslate();
        break;
      }
    }
  }

  public updateApiServiceValues(selectedValue: string): void {
    this.serviceSelected = selectedValue;

    switch (selectedValue) {
      case SERVICE_CODE.GOOGLE_CLOUD: {
        this._googleCloudServiceConfig();
        break;
      }

      case SERVICE_CODE.AMAZON: {
        this._amazonServiceConfig();
        break;
      }
    }
  }

  private _amazonServiceConfig(): void {
    this.languagesSupported = AMAZON_TRANSLATE_LANGUAGES.map(
      (amazonLanguage: AmazonLanguage) => ({name: amazonLanguage.language, value: amazonLanguage.languageCode})
    );
  }

  private _googleCloudServiceConfig(): void {
    this._googleCloudGetLanguagesService.get()
      .toPromise()
      .then((languagesResponse: GoogleCloudLanguagesResponse) => {
        const languages: GoogleCloudLanguageResponse [] = languagesResponse.data.languages;

        this.languagesSupported = languages.map(
          (googleLanguage: GoogleCloudLanguageResponse) => ({value: googleLanguage.language})
        );
      });
  }

  private _amazonServiceTranslate(): void {
    const translationRequest: AmazonTranslationRequestModel = {
      Text: this.inputText,
      SourceLanguageCode: 'auto',
      TargetLanguageCode: this.previewLanguageSelected
    };

    this._amazonTranslateService.translate(translationRequest)
      .toPromise()
      .then((translation: AmazonTranslationResponseModel) => {
        this.translatedText = translation.TranslatedText;
      })
      .catch((error: string) => {
        console.error(error);
      });
  }

  private _googleCloudServiceTranslate(): void {
    const translationRequest: GoogleCloudTranslateRequestModel = {
      model: 'nmt',
      q: [this.inputText],
      target: this.previewLanguageSelected
    };

    this._googleCloudTranslateService.translate(translationRequest)
      .toPromise()
      .then((translation: GoogleCloudTranslateResponseModel) => {
        const translations: GoogleCloudTranslationsResponseModel = translation.data.translations;

        this.translatedText = translations[0].translatedText;
      })
      .catch((error: string) => {
        console.error(error);
      });
  }

}
