export const AMAZON_TRANSLATE_LANGUAGES: AmazonLanguage [] = [
  {
    language: 'Afrikaans',
    languageCode: 'af'
  },
  {
    language: 'Albanian',
    languageCode: 'sq'
  },
  {
    language: 'Amharic',
    languageCode: 'am'
  },
  {
    language: 'Arabic',
    languageCode: 'ar'
  },
  {
    language: 'Azerbaijani',
    languageCode: 'az'
  },
  {
    language: 'Bengali',
    languageCode: 'bn'
  },
  {
    language: 'Bosnian',
    languageCode: 'bs'
  },
  {
    language: 'Bulgarian',
    languageCode: 'bg'
  },
  {
    language: 'Chinese (Simplified)',
    languageCode: 'zh'
  },
  {
    language: 'Chinese (Traditional)',
    languageCode: 'zh-TW'
  },
  {
    language: 'Croatian',
    languageCode: 'hr'
  },
  {
    language: 'Czech',
    languageCode: 'cs'
  },
  {
    language: 'Danish',
    languageCode: 'da'
  },
  {
    language: 'Dari',
    languageCode: 'fa-AF'
  },
  {
    language: 'Dutch',
    languageCode: 'nl'
  },
  {
    language: 'English',
    languageCode: 'en'
  },
  {
    language: 'Estonian',
    languageCode: 'et'
  },
  {
    language: 'Finnish',
    languageCode: 'fi'
  },
  {
    language: 'French',
    languageCode: 'fr'
  },
  {
    language: 'French (Canada)',
    languageCode: 'fr-CA'
  },
  {
    language: 'Georgian',
    languageCode: 'ka'
  },
  {
    language: 'German',
    languageCode: 'de'
  },
  {
    language: 'Greek',
    languageCode: 'el'
  },
  {
    language: 'Hausa',
    languageCode: 'ha'
  },
  {
    language: 'Hebrew',
    languageCode: 'he'
  },
  {
    language: 'Hindi',
    languageCode: 'hi'
  },
  {
    language: 'Hungarian',
    languageCode: 'hu'
  },
  {
    language: 'Indonesian',
    languageCode: 'id'
  },
  {
    language: 'Italian',
    languageCode: 'it'
  },
  {
    language: 'Japanese',
    languageCode: 'ja'
  },
  {
    language: 'Korean',
    languageCode: 'ko'
  },
  {
    language: 'Latvian',
    languageCode: 'lv'
  },
  {
    language: 'Malay',
    languageCode: 'ms'
  },
  {
    language: 'Norwegian',
    languageCode: 'no'
  },
  {
    language: 'Persian',
    languageCode: 'fa'
  },
  {
    language: 'Pashto',
    languageCode: 'ps'
  },
  {
    language: 'Polish',
    languageCode: 'pl'
  },
  {
    language: 'Portuguese',
    languageCode: 'pt'
  },
  {
    language: 'Romanian',
    languageCode: 'ro'
  },
  {
    language: 'Russian',
    languageCode: 'ru'
  },
  {
    language: 'Serbian',
    languageCode: 'sr'
  },
  {
    language: 'Slovak',
    languageCode: 'sk'
  },
  {
    language: 'Slovenian',
    languageCode: 'sl'
  },
  {
    language: 'Somali',
    languageCode: 'so'
  },
  {
    language: 'Spanish',
    languageCode: 'es'
  },
  {
    language: 'Spanish (Mexico)',
    languageCode: 'es-MX'
  },
  {
    language: 'Swahili',
    languageCode: 'sw'
  },
  {
    language: 'Swedish',
    languageCode: 'sv'
  },
  {
    language: 'Tagalog',
    languageCode: 'tl'
  },
  {
    language: 'Tamil',
    languageCode: 'ta'
  },
  {
    language: 'Thai',
    languageCode: 'th'
  },
  {
    language: 'Turkish',
    languageCode: 'tr'
  },
  {
    language: 'Ukrainian',
    languageCode: 'uk'
  },
  {
    language: 'Urdu',
    languageCode: 'ur'
  },
  {
    language: 'Vietnamese',
    languageCode: 'vi'
  }
];

export interface AmazonLanguage {

  language: string;

  languageCode: string;

}
