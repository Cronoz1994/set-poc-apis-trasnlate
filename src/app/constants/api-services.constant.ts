
export enum SERVICE_CODE {

  GOOGLE_CLOUD = 'google-cloud-service',

  AMAZON = 'amazon-service'

}

export const API_SERVICES: ApiService [] = [
  {
    name: 'Google Cloud Translate',
    value: SERVICE_CODE.GOOGLE_CLOUD
  },
  {
    name: 'Amazon translate',
    value: SERVICE_CODE.AMAZON
  }
];

export interface ApiService {

  name: string;

  value: SERVICE_CODE;

}
