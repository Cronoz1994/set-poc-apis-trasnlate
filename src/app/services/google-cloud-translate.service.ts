import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {GoogleCloudTranslateResponseModel} from '../models/google-cloud-translate-response.model';
import {GoogleCloudTranslateRequestModel} from '../models/google-cloud-translate-request.model';

@Injectable({
  providedIn: 'root'
})
export class GoogleCloudTranslateService {

  private readonly _HOST = 'translation.googleapis.com/language/translate/v2';

  private readonly _KEY = '_API_KEY';

  private readonly _ENDPOINT = `https://${this._HOST}?key=${this._KEY}`;

  constructor(private http: HttpClient) {
  }

  public translate(request: GoogleCloudTranslateRequestModel): Observable<GoogleCloudTranslateResponseModel> {
    return this.http.post<GoogleCloudTranslateResponseModel>(this._ENDPOINT, request);
  }

}
