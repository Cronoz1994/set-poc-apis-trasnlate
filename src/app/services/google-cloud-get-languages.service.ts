import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GoogleCloudLanguagesResponse} from '../models/google-cloud-language.response';

@Injectable({
  providedIn: 'root'
})
export class GoogleCloudGetLanguagesService {

  private readonly _HOST = 'translation.googleapis.com/language/translate/v2/languages';

  private readonly _KEY = '_API_KEY';

  private readonly _ENDPOINT = `https://${this._HOST}?key=${this._KEY}`;

  constructor(private http: HttpClient) {
  }

  public get(): Observable<GoogleCloudLanguagesResponse> {
    return this.http.get<GoogleCloudLanguagesResponse>(this._ENDPOINT);
  }

}
