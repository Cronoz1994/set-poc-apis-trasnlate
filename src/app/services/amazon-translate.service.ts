import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

import * as CryptoJS from 'crypto-js';

import {AmazonTranslationRequestModel} from '../models/amazon-translation-request.model';
import {AmazonTranslationResponseModel} from '../models/amazon-translation-response.model';

@Injectable({
  providedIn: 'root'
})
export class AmazonTranslateService {

  private _METHOD = 'POST';
  private _SERVICE = 'translate';
  private _REGION = 'us-east-2';
  private _HOST = `${this._SERVICE}.${this._REGION}.amazonaws.com`;
  private _ENDPOINT = `https://${this._HOST}/`;

  private _AMAZON_TARGET = 'AWSShineFrontendService_20170701.TranslateText';
  private _CONTENT_TYPE = 'application/x-amz-json-1.1';

  private _ACCESS_KEY = '_ACCESS_KEY';
  private _SECRET_KEY = '_SECRET_KEY';

  private _ALGORITHM = 'AWS4-HMAC-SHA256';
  private _SIGNED_HEADERS = 'content-type;host;x-amz-date;x-amz-target';

  private _amazonDateTime: string;
  private _credentialScope: string;
  private _dateStamp: string;

  constructor(private _httpClient: HttpClient) {
  }

  public translate(translationRequest: AmazonTranslationRequestModel): Observable<AmazonTranslationResponseModel> {
    this._amazonDateTime = (new Date).toISOString().replace(/-|:|\..{3}/g, '');
    this._dateStamp = this._amazonDateTime.substr(0, 8);

    const canonicalRequest: string = this._buildCanonicalRequest(translationRequest);
    const stringToSign: string = this._buildStringToSign(canonicalRequest);
    const signature: string = this._calculateTheSignature(stringToSign);

    const headers: HttpHeaders = this._buildHeaders(signature, translationRequest);

    return this._httpClient.post<AmazonTranslationResponseModel>(this._ENDPOINT, translationRequest, {headers: headers});
  }

  private _getSignatureKey(key, dateStamp, regionName, serviceName): string {
    const kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4' + key);
    const kRegion = CryptoJS.HmacSHA256(regionName, kDate);
    const kService = CryptoJS.HmacSHA256(serviceName, kRegion);
    const kSigning = CryptoJS.HmacSHA256('aws4_request', kService);

    return kSigning;
  }

  private _buildCanonicalRequest(request: AmazonTranslationRequestModel): string {
    const canonicalUri = '/';
    const canonicalQueryString = '';

    const canonicalHeaders =
      `content-type:${this._CONTENT_TYPE}\n` +
      `host:${this._HOST}\n` +
      `x-amz-date:${this._amazonDateTime}\n` +
      `x-amz-target:${this._AMAZON_TARGET}\n`;

    const payloadHash: string = CryptoJS.SHA256(JSON.stringify(request)).toString();

    const canonicalRequest =
      `${this._METHOD}\n` +
      `${canonicalUri}\n` +
      `${canonicalQueryString}\n` +
      `${canonicalHeaders}\n` +
      `${this._SIGNED_HEADERS}\n` +
      payloadHash;

    return canonicalRequest;
  }

  private _buildStringToSign(canonicalRequest: string): string {
    this._credentialScope = `${this._dateStamp}/${this._REGION}/${this._SERVICE}/aws4_request`;

    const stringToSign =
      `${this._ALGORITHM}\n` +
      `${this._amazonDateTime}\n` +
      `${this._credentialScope}\n` +
      CryptoJS.SHA256(canonicalRequest).toString();

    return stringToSign;
  }

  private _calculateTheSignature(stringToSign: string): string {
    const signingKey: string = this._getSignatureKey(this._SECRET_KEY, this._dateStamp, this._REGION, this._SERVICE);
    const signature: string = CryptoJS.HmacSHA256(stringToSign, signingKey).toString();

    return signature;
  }

  private _buildHeaders(signature: string, request: any): HttpHeaders {
    const authorizationHeader =
      `${this._ALGORITHM} ` +
      `Credential=${this._ACCESS_KEY}/${this._credentialScope}, ` +
      `SignedHeaders=${this._SIGNED_HEADERS}, ` +
      `Signature=${signature}`;

    const headers = new HttpHeaders({
      'Content-Type': this._CONTENT_TYPE,
      'X-Amz-Date': this._amazonDateTime,
      'X-Amz-Target': this._AMAZON_TARGET,
      'Authorization': authorizationHeader
    });

    return headers;
  }

}
