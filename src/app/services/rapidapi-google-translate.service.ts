import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {RapidapiGoogleTranslationRequestModel} from '../models/rapidapi-google-translation-request.model';
import {Observable} from 'rxjs';
import {RapidapiGoogleTranslationResponseModel} from '../models/rapidapi-google-translation-response.model';

@Injectable({
  providedIn: 'root'
})
export class RapidapiGoogleTranslateService {

  private _URL = 'https://google-translate1.p.rapidapi.com/language/translate/v2';

  private _KEY = 'a7a7dd7ea8msh974fc5f046167e5p145dd2jsn5887ac5cd62b';

  constructor(private _httpClient: HttpClient) {
  }

  public translate(translationRequest: RapidapiGoogleTranslationRequestModel): Observable<RapidapiGoogleTranslationResponseModel> {
    const headers = new HttpHeaders({
      'X-RapidAPI-Host': 'google-translate1.p.rapidapi.com',
      'X-RapidAPI-Key': this._KEY,
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this._httpClient.post<RapidapiGoogleTranslationResponseModel>(this._URL, translationRequest, {headers: headers});
  }

}
