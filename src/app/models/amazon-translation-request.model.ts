export interface AmazonTranslationRequestModel {

  SourceLanguageCode: string;

  TargetLanguageCode: string;

  TerminologyNames?: string [];

  Text: string;

}
