export interface RapidapiGoogleTranslationRequestModel {

  format?: string;

  model?: string;

  q: string;

  source?: string;

  target: string;


}
