export interface RapidapiGoogleTranslationResponseModel {

  data: { [translations: string]: RapidapiGoogleTranslationModel [] };

}

export interface RapidapiGoogleTranslationModel {

  translatedText: string;

}
