export interface AmazonTranslationResponseModel {

  AppliedTerminologies: AmazonTranslationAppliedTerminologies [];

  SourceLanguageCode: string;

  TargetLanguageCode: string;

  TranslatedText: string;

}

export interface AmazonTranslationAppliedTerminologies {

  Name: string;

  Terms: AmazonTranslationTerms [];

}

export interface AmazonTranslationTerms {

  SourceText: string;

  TargetText: string;

}
