
export interface GoogleCloudTranslateResponseModel {

  data: { [translations: string]: GoogleCloudTranslationsResponseModel };

}

export interface GoogleCloudTranslationsResponseModel {

  translations: GoogleCloudTranslationResponseModel [];

}

export interface GoogleCloudTranslationResponseModel {

  translatedText: string;

  detectedSourceLanguage: string;

}
