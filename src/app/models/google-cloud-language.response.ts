
export interface GoogleCloudLanguagesResponse {

  data: { languages: GoogleCloudLanguageResponse [] };

}

export interface GoogleCloudLanguageResponse {

  language: string;

}
